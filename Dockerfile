FROM ubuntu:latest
COPY . /build
RUN apt-get install -y wine wget p7zip sed dos2unix
RUN unix2dos *.txt
RUN unix2dos files_example/*.txt files_example/*/*.txt
RUN sed -i 's/..\\vcpkg\\installed\\/.\\protobuf_/g' build_set_protobuf_directories.bat
#RUN wget 'https://gitlab.com/Mr_Goldberg/goldberg_emulator/uploads/48db8f434a193aae872279dc4f5dde6a/sdk_standalone.7z'
#RUN wget 'https://gitlab.com/Mr_Goldberg/goldberg_emulator/uploads/0119304e030098b4821d73170fe52084/protobuf_x64-windows-static.7z'
#RUN wget 'https://gitlab.com/Mr_Goldberg/goldberg_emulator/uploads/4185a97ab363ddc1859127e59ec68581/protobuf_x86-windows-static.7z'
RUN 7za x protobuf_x86-windows-static.7z -oprotobuf_x86-windows-static
RUN 7za x protobuf_x64-windows-static.7z -oprotobuf_x64-windows-static
RUN 7za x sdk_standalone.7z -osdk_standalone
RUN DLL_FILES="$(ls dll/*.cpp | tr "\n" " ")"; sed "s|dll/\*.cpp|$DLL_FILES|g" -i *.bat
RUN DLL_FILES="$(ls detours/*.cpp | tr "\n" " ")"; sed "s|detours/\*.cpp|$DLL_FILES|g" -i *.bat
RUN DLL_FILES="$(ls overlay_experimental/*.cpp | tr "\n" " ")"; sed "s|overlay_experimental/\*.cpp|$DLL_FILES|g" -i *.bat
RUN DLL_FILES="$(ls overlay_experimental/windows/*.cpp | tr "\n" " ")"; sed "s|overlay_experimental/windows/\*.cpp|$DLL_FILES|g" -i *.bat
RUN DLL_FILES="$(ls ImGui/*.cpp | tr "\n" " ")"; sed "s|ImGui/\*.cpp|$DLL_FILES|g" -i *.bat
RUN DLL_FILES="$(ls ImGui/impls/*.cpp | tr "\n" " ")"; sed "s|ImGui/impls/\*.cpp|$DLL_FILES|g" -i *.bat
RUN DLL_FILES="$(ls ImGui/impls/windows/*.cpp | tr "\n" " ")"; sed "s|ImGui/impls/windows/\*.cpp|$DLL_FILES|g" -i *.bat
RUN DLL_FILES="$(ls dll/*.proto | tr "\n" " " | sed "s/.proto/.pb.cc/g")"; sed "s|dll/\*.cc|$DLL_FILES|g" -i *.bat
RUN DLL_FILES="$(ls steamclient_loader/*.cpp | tr "\n" " ")"; sed "s|steamclient_loader/\*.cpp|$DLL_FILES|g" -i *.bat
RUN sed "s| /MP12 | /MP4 |g" -i *.bat
RUN export WINEDEBUG=-all
RUN wine cmd /c build_win_debug_experimental.bat
RUN wine cmd /c build_win_release.bat
RUN mkdir release/debug_experimental
RUN mv steam_api.dll steam_api64.dll release/debug_experimental/
RUN rm -f steamclient.dll steamclient64.dll
RUN wine cmd /c build_win_debug_experimental_steamclient.bat
RUN mkdir release/debug_experimental_steamclient
RUN mv steamclient.dll steamclient64.dll release/debug_experimental_steamclient/
RUN cp Readme_debug.txt release/debug_experimental/Readme.txt


 
